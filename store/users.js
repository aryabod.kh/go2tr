export const state = () => ({
    users: [
        {
            user: 'admin',
            password: 'admin'
        },
        {
            user: 'user',
            password: 'user'
        }
    ],
    user: null
})
export const getters = {

}
export const mutations = {
    login_admin(state) {
        state.user = {
            name: 'admin',
            access: true
        }
    },
    login_user(state) {
        state.user = {
            name: 'user',
            access: false
        }
    }
}
export const actions = {
    login({ commit }, login) {
        debugger
        console.log(login);
        if (login.user == 'admin') {
            commit('login_admin')
        } else if (login.user == 'user') {
            commit('login_user')
        }
    },
    logout({ state }) {
        state.user = null
    }
}
