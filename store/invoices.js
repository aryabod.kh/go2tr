export const state = () => ({
    invoices: [
        {
            id: 1,
            price: 150,
            comment: 'Lorep ipsum'
        },
        {
            id: 2,
            price: 180,
            comment: 'Lorep ipsum'
        },
        {
            id: 3,
            price: 150,
            comment: 'Lorep ipsum'
        },
        {
            id: 4,
            price: 180,
            comment: 'Lorep ipsum'
        }
    ],
})
export const getters = {

}
export const mutations = {

}
export const actions = {
    Update({ state }, selected_invoice) {
        debugger
        state.invoices[selected_invoice.id - 1] = Object.assign(selected_invoice)
    },
    Add({ state }, selected_invoice) {
        let invoice = {
            id: state.invoices.length + 1,
            price: selected_invoice.price,
            comment: selected_invoice.comment
        }
        state.invoices.push(invoice)
    },
}
